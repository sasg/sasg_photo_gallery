<?php
/**
 * @file
 * sasg_photo_gallery.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function sasg_photo_gallery_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_image_default_styles().
 */
function sasg_photo_gallery_image_default_styles() {
  $styles = array();

  // Exported image style: gallery_grid.
  $styles['gallery_grid'] = array(
    'label' => 'Gallery grid',
    'effects' => array(
      10 => array(
        'name' => 'focal_point_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 360,
          'focal_point_advanced' => array(
            'shift_x' => '',
            'shift_y' => '',
          ),
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: gallery_large.
  $styles['gallery_large'] = array(
    'label' => 'Gallery large',
    'effects' => array(
      9 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 800,
          'height' => 800,
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}

/**
 * Implements hook_node_info().
 */
function sasg_photo_gallery_node_info() {
  $items = array(
    'sasg_photo_gallery' => array(
      'name' => t('Photo Gallery'),
      'base' => 'node_content',
      'description' => t('Use the <em>photo gallery</em> content type to create a collection of photos.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

/**
 * Implements hook_paragraphs_info().
 */
function sasg_photo_gallery_paragraphs_info() {
  $items = array(
    'sasg_gallery_photos' => array(
      'name' => 'Gallery photos',
      'bundle' => 'sasg_gallery_photos',
      'locked' => '1',
    ),
  );
  return $items;
}
