# SASG Photo Gallery

Creates a photo gallery content type.

### Drupal Contrib Modules

- [Chaos tools](https://www.drupal.org/project/ctools)
- [Display Suite](https://www.drupal.org/project/ds)
- [Features](https://www.drupal.org/project/features)
- [Focal Point](https://www.drupal.org/project/focal_point)
- [Entity API](https://www.drupal.org/project/entity)
- [Image formatter link to image style](https://www.drupal.org/project/image_formatter_link_to_image_style)
- [Jquery update](https://www.drupal.org/project/jquery_update)
- [Libraries](https://www.drupal.org/project/libraries)
- [Paragraphs](https://www.drupal.org/project/paragraphs)
- [Strongarm](https://www.drupal.org/project/strongarm)

### Libraries

- [Magnific Popup](http://dimsemenov.com/plugins/magnific-popup/)

### Additional Configuration

- For now, Enable Field Templates in Display Suite is manually activated. Go to admin/structure/ds/list/extras, check Enable Field Templates and Save configuration.

### Issues

- [Entity API patch](https://www.drupal.org/node/2807275)
