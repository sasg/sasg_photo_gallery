<?php
/**
 * @file
 * sasg_photo_gallery.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function sasg_photo_gallery_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-sasg_photo_gallery-field_sasg_gallery_photo_desc'.
  $field_instances['node-sasg_photo_gallery-field_sasg_gallery_photo_desc'] = array(
    'bundle' => 'sasg_photo_gallery',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_sasg_gallery_photo_desc',
    'label' => 'Gallery Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'node-sasg_photo_gallery-field_sasg_gallery_photos'.
  $field_instances['node-sasg_photo_gallery-field_sasg_gallery_photos'] = array(
    'bundle' => 'sasg_photo_gallery',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'paragraphs',
        'settings' => array(
          'view_mode' => 'full',
        ),
        'type' => 'paragraphs_view',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'node',
    'field_name' => 'field_sasg_gallery_photos',
    'label' => 'Photo(s)',
    'required' => 1,
    'settings' => array(
      'add_mode' => 'select',
      'allowed_bundles' => array(
        'sasg_gallery_photos' => 'sasg_gallery_photos',
      ),
      'bundle_weights' => array(
        'sasg_gallery_photos' => 2,
      ),
      'default_edit_mode' => 'open',
      'title' => 'photo',
      'title_multiple' => 'photos',
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 0,
      'module' => 'paragraphs',
      'settings' => array(),
      'type' => 'paragraphs_embed',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_gallery_photos-field_sasg_gallery_caption'.
  $field_instances['paragraphs_item-sasg_gallery_photos-field_sasg_gallery_caption'] = array(
    'bundle' => 'sasg_gallery_photos',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_sasg_gallery_caption',
    'label' => 'Caption',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'paragraphs_item-sasg_gallery_photos-field_sasg_gallery_photo'.
  $field_instances['paragraphs_item-sasg_gallery_photos-field_sasg_gallery_photo'] = array(
    'bundle' => 'sasg_gallery_photos',
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image_formatter_link_to_image_style',
        'settings' => array(
          'image_class' => '',
          'image_link_style' => 'gallery_large',
          'image_style' => 'gallery_grid',
          'link_class' => '',
          'link_rel' => '',
        ),
        'type' => 'image_formatter_link_to_image_style',
        'weight' => 0,
      ),
      'paragraphs_editor_preview' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'ds_extras_field_template' => '',
    'entity_type' => 'paragraphs_item',
    'field_name' => 'field_sasg_gallery_photo',
    'label' => 'Photo',
    'required' => 1,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 1,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Gallery Description');
  t('Photo');
  t('Photo(s)');

  return $field_instances;
}
